class Employee {
  String name;
  String email;
  String address;
  int salary;
  Employee(String name, String email, String address, int salary) {
    this.name = name;
    this.email = email;
    this.address = address;
    this.salary = salary;
  }
}

void main() {
  Employee employee01 =
      Employee("Pratama Wisnu", "pratama@gmail.com", "Solo", 1000000);
  print(employee01.name);
}
